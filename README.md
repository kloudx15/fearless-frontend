# Project Conference GO
Conference Go is a beginner project that helped me understand the uses and ways to utilize javascript and React. Not
only did this project help me grab the ropes of the library but it also introduced different methods of delivering data to several microservices. Polling, Queueing, and Pub/sub are used throughout the services to retrieve data from each other.

# Uses of APIs
The use of APIs are present within this project. They are the pexels.com api for images presented on each card in the home page. Another is the weather api where the weather for a particular city is given when an attendee wants to attend a conference.

# Forms 
Several Forms are made for the admin or users who want to create a location, conference, or an account. These forms are created in jsx format. It was challenging to implement React principles during this step, but after reading the documentation and watching tutorials, the on change and submit components became easier to implement.

# Use of Bootstrap
The introduction of Bootstrap was really fun as seeing the changes being made was rewarding. I hope to use Bootstrap more in the future when creating layouts and forms.