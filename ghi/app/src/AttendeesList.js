function AttendeesList(props) {
    return (
        <table className="table table-hover">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Conference</th>
            </tr>
            </thead>
            <tbody className="table-secondary">
            {props.attendees.map(attendee => {
            return (
            <tr key={attendee.href}>
                <td className="table-secondary">{ attendee.name }</td>
                <td className="table-secondary">{ attendee.conference }</td>
            </tr>
            );
            })}
            </tbody>
        </table>
    );
}
export default AttendeesList;
